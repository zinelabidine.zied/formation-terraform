resource "null_resource" "ssh_target" {
  connection {
    type        = "ssh"
    user        = var.ssh_user
    host        = var.ssh_host
    private_key = file(var.ssh_key)
  }
}

terraform {
  required_providers {
    docker = {
      source  = "kreuzwerker/docker"
      version = "2.15.0"
    }
  }
}

provider "docker" {
  host = "tcp://${var.ssh_host}:2375"
}



resource "docker_network" "app" {
  name = "app_net"
}


resource "docker_container" "db" {
  name  = "db"
  image = "mysql:5.7"
  restart = "always"
  network_mode = "app_net"
  env = [
     "MYSQL_ROOT_PASSWORD=password",
     "MYSQL_PASSWORD=password",
     "MYSQL_USER=appphp",
     "MYSQL_DATABASE=appphp"
  ]
  networks_advanced {
    name = docker_network.app.name
  }
}


resource "docker_container" "app_php" {
  name  = "app_php"
  image = "formationsk8s/tp_php_mysql:latest"
  restart = "always"
  env = [
    "MYSQL_HOST=db",
    "MYSQL_PORT=3306",
    "MYSQL_USER=root",
    "MYSQL_PASSWORD=password",
    "MYSQL_DB=appphp"
  ]
  ports {
    internal = 80
    external = var.app_port
  }
}
