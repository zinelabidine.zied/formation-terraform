variable "ssh_host" {}
variable "ssh_user" {}
variable "ssh_key" {}
resource "null_resource" "ssh_target" {
  connection {
    type        = "ssh"
    user        = var.ssh_user
    host        = var.ssh_host
    private_key = file(var.ssh_key)
  }
   provisioner "remote-exec" {
    inline = [
      "rm -rf terrform-docker",
      "mkdir terrform-docker"

    ]
  }
 provisioner "file" {
    source      = "index.html"
    destination = "terrform-docker/index.html"
  }
   provisioner "file" {
    source      = "Dockerfile"
    destination = "terrform-docker/Dockerfile"
  }

  provisioner "remote-exec" {
    inline = [
      "cd terrform-docker",
      "sudo docker build -t formation .",
      "sudo docker run -d -p -v /var/www/html:/var/www/html 80:80 formation"
    ]
  }

}
output "host" {
value = var.ssh_host
}
output "user" {
value = var.ssh_user
}
output "key" {
value = var.ssh_key
}

