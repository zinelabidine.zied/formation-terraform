variable "ssh_host" {}
variable "ssh_user" {}
variable "ssh_key" {}

module "install" {
  source        = "./modules/docker_install"
  ssh_host      = var.ssh_host
  ssh_key       = var.ssh_key
  ssh_user      = var.ssh_user
}


module "docker_run_ngnix" {
  source        = "./modules/docker_run_ngnix"
  ssh_host      = var.ssh_host
}

