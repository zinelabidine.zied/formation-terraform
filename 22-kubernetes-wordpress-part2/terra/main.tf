variable "mysql-password" {}
variable "mysql-img" {}
variable "mysql-dir" {}
variable "mysql-lbl" {}
variable "wordpress-port" {}
variable "wordpress-img" {}
variable "wordpress-host" {}
variable "wordpress-dir" {}
variable "wordpress-lbl" {}


provider "kubernetes" {
  config_path = "~/.kube/config"
}

resource "kubernetes_persistent_volume" "wp-pv-mysql" {
  metadata {
    name = "pv-mysql"
  }
  spec {
    capacity = {
      storage = "2Gi"
    }
    access_modes = ["ReadWriteMany"]
    storage_class_name = "standard"
    persistent_volume_source {
      host_path {
        path = var.mysql-dir
      }
    }
  }
}

resource "kubernetes_persistent_volume" "wp-pv-wordpress" {
  metadata {
    name = "pv-wordpress"
  }
  spec {
    capacity = {
      storage = "2Gi"
    }
    access_modes = ["ReadWriteMany"]
    storage_class_name = "standard"
    persistent_volume_source {
      host_path {
        path = var.wordpress-dir
      }
    }
  }
}

resource "kubernetes_persistent_volume_claim" "wp-pvc-mysql" {
  metadata {
    name = "pvc-mysql"
  }
  spec {
    access_modes = ["ReadWriteMany"]
    storage_class_name = "standard"
    resources {
      requests = {
        storage = "2Gi"
      }
    }
    volume_name = kubernetes_persistent_volume.wp-pv-mysql.metadata[0].name
  }
}

resource "kubernetes_persistent_volume_claim" "wp-pvc-wordpress" {
  metadata {
    name = "pvc-wordpress"
  }
  spec {
    access_modes = ["ReadWriteMany"]
    storage_class_name = "standard"
    resources {
      requests = {
        storage = "2Gi"
      }
    }
    volume_name = kubernetes_persistent_volume.wp-pv-wordpress.metadata[0].name
  }
}

resource "kubernetes_secret" "mysql" {
  metadata {
    name = "mysql-pass"
  }
  data = {
    password = var.mysql-password
  }
}

resource "kubernetes_deployment" "wp-dep-mysql" {
  metadata {
    name = "wp-dep-mysql"
    labels = {
      app = var.mysql-lbl
    }
  }

  spec {
    replicas = 1

    selector {
      match_labels = {
        app = var.mysql-lbl
      }
    }

    template {
      metadata {
        labels = {
          app = var.mysql-lbl
        }
      }

      spec {
        container {
          image = var.mysql-img
          name  = "wp-mysql"
         env {
           name = "MYSQL_ROOT_PASSWORD"
           value_from {
             secret_key_ref {
               name = kubernetes_secret.mysql.metadata.0.name
               key = "password"
             }
           }
          }
        env {
           name= "MYSQL_USER"
          value= "wpuser"
         }
        env {
          name= "MYSQL_PASSWORD"
          value= "test"
        }
       env {
          name= "MYSQL_DATABASE"
          value= "test"
        }
          port {
            container_port = 3306
            name = "mysql"
          }
        }
        }
      }
    }
  }

resource "kubernetes_service" "wp-svc-mysql" {
  metadata {
    name = "wp-mysql"
  }
  spec {
    selector = {
      app = var.mysql-lbl
    }
    port {
      port = 3306
    }
  }
}


resource "kubernetes_deployment" "wp-dep-wordpress" {
  metadata {
    name = "wp-dep-wordpress"
    labels = {
      app = var.wordpress-lbl
    }
  }

  spec {
    replicas = 1

    selector {
      match_labels = {
        app = var.wordpress-lbl
      }
    }

    template {
      metadata {
        labels = {
          app = var.wordpress-lbl
        }
      }

      spec {
        container {
          image = var.wordpress-img
          name  = "wordpress"
          env {
            name = "WORDPRESS_DB_HOST"
            value = "wp-mysql"
          }
          env {
            name = "WORDPRESS_DB_USER"
            value = "root"
            }
        env {
           name = "WORDPRESS_DB_PASSWORD"
           value_from {
             secret_key_ref {
               name = kubernetes_secret.mysql.metadata.0.name
               key = "password"
             }
           }
          }

        env {
           name = "WORDPRESS_DB_NAME"
           value = "test"
           }
           env {
            name = "WP_ALLOW_REPAIR"
            value = true
          }

          port {
            container_port = var.wordpress-port
            name = "wordpress"
          }
          volume_mount {
            name = "wordpress-persistent-storage"
            mount_path = "/var/www/html"
          }
        }
        volume {
          name = "wordpress-persistent-storage"
          persistent_volume_claim {
            claim_name = kubernetes_persistent_volume_claim.wp-pvc-wordpress.metadata.0.name
          }
        }

      }
    }
  }
}


resource "kubernetes_service" "wp-svc-wordpress" {
  metadata {
    name = "wp-svc-wordpress"
  }
  spec {
    selector = {
      app = var.wordpress-lbl
    }
    port {
      port = var.wordpress-port
    }

    type = "NodePort"
  }
}

resource "kubernetes_ingress_v1" "wp-ingress" {
  metadata {
    name = "webserver-ingress"
     annotations = {
      "nginx.ingress.kubernetes.io/rewrite-target" = "/"
    }
  }

 spec {
      rule {
      http {
        path {
          backend {
            service {
              name = kubernetes_service.wp-svc-wordpress.metadata[0].name
              port {
                number = var.wordpress-port
              }
            }
          }

          path = "/"
        }
      }
    }
  }
}

